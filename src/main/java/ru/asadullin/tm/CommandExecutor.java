package ru.asadullin.tm;

/**
 * Executor class with map including all known commands
 */
public class CommandExecutor {
    private CommandExecutor() {
    }

    public static Operation execute(String string) {
        Operation operation = null;
        for (Operation op : Operation.values()) {
            if (op.getName().equalsIgnoreCase(string)) {
                operation = op;
                operation.getCommand().execute();
                break;
            }
        }
        if (operation == null) System.out.println("Wrong command, please type \"help\" for a list of commands.");
        return operation;
    }
}
