package ru.asadullin.tm;

import ru.asadullin.tm.command.Command;
import ru.asadullin.tm.command.defaultCommand.ExitCommand;
import ru.asadullin.tm.command.defaultCommand.HelpCommand;
import ru.asadullin.tm.command.project.*;
import ru.asadullin.tm.command.task.*;

/**
 * Enum for operations
 */
public enum Operation {
    HELP("help", "Show all commands.", new HelpCommand()),
    PROJECT_SELECT("project-select", "Select project by name.", new ProjectSelectCommand()),
    PROJECT_DESELECT("project-deselect", "Deselect last project.", new ProjectDeselectCommand()),
    PROJECT_EDIT("project-edit", "Edit selected project.", new ProjectEditCommand()),
    PROJECT_CLEAR("project-clear", "Remove all projects.", new ProjectClearCommand()),
    PROJECT_CREATE("project-create", "Create new project.", new ProjectCreateCommand()),
    PROJECT_LIST("project-list", "Show all projects.", new ProjectListCommand()),
    PROJECT_DELETE("project-delete", "Delete project.", new ProjectDeleteCommand()),
    TASK_SELECT("task-select", "Select project by name.", new TaskSelectCommand()),
    TASK_DESELECT("task-deselect", "Deselect last task.", new TaskDeselectCommand()),
    TASK_EDIT("task-edit", "Edit selected project.", new TaskEditCommand()),
    TASK_CLEAR("task-clear", "Remove all tasks.", new TaskClearCommand()),
    TASK_CREATE("task-create", "Create new task.", new TaskCreateCommand()),
    TASK_LIST("task-list", "Show all tasks.", new TaskListCommand()),
    TASK_DELETE("task-delete", "Delete task.", new TaskDeleteCommand()),
    EXIT("exit", "Exit the Task manager.", new ExitCommand());

    private final String name;
    private final String description;
    private final Command command;

    Operation(String name, String description, Command command) {
        this.name = name;
        this.description = description;
        this.command = command;
    }

    public String getName() {
        return name;
    }

    public Command getCommand() {
        return command;
    }

    @Override
    public String toString() {
        return name + ": " + description;
    }
}
