package ru.asadullin.tm.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * utility class help with console
 */
public class ConsoleHelper {
    private static final BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));

    public static String readString() {
        String mes = null;
        try {
            mes = bis.readLine();
        } catch (IOException e) {
            System.out.println("Произошла ошибка при попытке ввода текста. Попробуйте еще раз.");
            mes = readString();
        }
        return mes;
    }

    public static int readInt() throws NumberFormatException {
        return Integer.parseInt(readString().trim());
    }

    public static boolean confirmOperation() {
        System.out.println("Confirm operation: y/n");
        while (true) {
            String answer = readString();
            if (answer.equalsIgnoreCase("y")) return true;
            else if (answer.equalsIgnoreCase("n")) return false;
            else System.out.println("Wrong command, please type \"y\" for Yes, or \"n\" for No");
        }
    }
}
