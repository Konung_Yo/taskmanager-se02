package ru.asadullin.tm.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * utility class help with date
 */
public class DateHelper {
    public static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");

    public static String formatDateTime(LocalDateTime dateTime) {
        return dtf.format(dateTime);
    }
}
