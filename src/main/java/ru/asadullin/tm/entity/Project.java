package ru.asadullin.tm.entity;

import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class for working with the "project" entity
 */
public class Project {
    private final static AtomicInteger idSequence = new AtomicInteger();
    private final int id;
    private String name;
    private String description;
    private final LocalDateTime createDate;

    public Project(String name, String description) {
        this.id  = idSequence.incrementAndGet();
        this.name = name;
        this.description = description;
        this.createDate = LocalDateTime.now();
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }
}
