package ru.asadullin.tm.entity;

import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class for working with the "task" entity
 */
public class Task {
    private final static AtomicInteger idSequence = new AtomicInteger();
    private final int id;
    private String name;
    private String description;
    private final Project project;
    private final LocalDateTime createDate;
    public Task(String name, String description, Project project) {
        this.id  = idSequence.incrementAndGet();
        this.name = name;
        this.project = project;
        this.description = description;
        this.createDate = LocalDateTime.now();
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Project getProject() {
        return project;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }
}
