package ru.asadullin.tm;

import ru.asadullin.tm.utils.ConsoleHelper;

/**
 * Main class
 */
public class TaskManager {
    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        Operation operation = null;
        do {
            try {
                operation = CommandExecutor.execute(ConsoleHelper.readString().trim());
            } catch (Exception e) {
                System.out.println("Something goes wrong");
                e.printStackTrace();
            }
        } while (operation != Operation.EXIT);
    }

}




