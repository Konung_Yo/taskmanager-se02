package ru.asadullin.tm;

import ru.asadullin.tm.entity.Project;
import ru.asadullin.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Singleton class with data
 */
public class Storage {
    private final List<Project> projects;
    private final List<Task> tasks;
    private Project selectedProject;
    private Task selectedTask;
    private static volatile Storage instance;
    public static Storage getInstance() {
        Storage localInstance = instance;
        if (localInstance == null) {
            synchronized (Storage.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new Storage();
                }
            }
        }
        return localInstance;
    }

    private Storage() {
        this.projects = new ArrayList<>();
        this.tasks = new ArrayList<>();
    }

    public List<Project> getProjects() {
        return projects;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public Project getSelectedProject() {
        return selectedProject;
    }

    public void setSelectedProject(Project selectedProject) {
        this.selectedProject = selectedProject;
    }

    public Task getSelectedTask() {
        return selectedTask;
    }

    public void setSelectedTask(Task selectedTask) {
        this.selectedTask = selectedTask;
    }

    public void deselectTask() {
        this.selectedTask = null;
    }

    public void deselectProject() {
        this.selectedProject = null;
    }
}