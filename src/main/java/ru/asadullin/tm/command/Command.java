package ru.asadullin.tm.command;

/**
 * Execute interface
 */
public interface Command {
    void execute();
}
