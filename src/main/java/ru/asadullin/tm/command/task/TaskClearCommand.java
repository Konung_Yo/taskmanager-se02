package ru.asadullin.tm.command.task;

import ru.asadullin.tm.Storage;
import ru.asadullin.tm.command.Command;
import ru.asadullin.tm.utils.ConsoleHelper;

/**
 * Command clears taskList in storage
 */
public class TaskClearCommand implements Command {
    @Override
    public void execute() {
        Storage storage = Storage.getInstance();
        System.out.println(" [CLEAR TASKS]");
        if (ConsoleHelper.confirmOperation()) {
            storage.getTasks().clear();
            System.out.println(" [ALL TASKS REMOVED]");
        } else {
            System.out.println(" [OPERATION CANCELLED]");
        }
    }
}
