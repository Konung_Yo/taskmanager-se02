package ru.asadullin.tm.command.task;

import ru.asadullin.tm.Storage;
import ru.asadullin.tm.command.Command;
import ru.asadullin.tm.entity.Project;
import ru.asadullin.tm.entity.Task;
import ru.asadullin.tm.utils.ConsoleHelper;

import java.util.Comparator;
import java.util.List;

/**
 * Command create new task entity and put it in taskList in storage
 */
public class TaskCreateCommand implements Command {
    @Override
    public void execute() {
        System.out.println(" [TASK CREATE]");
        Storage storage = Storage.getInstance();
        List<Project> projectList = storage.getProjects();
        if (projectList.isEmpty()) {
            System.out.println(" [NO SUCH PROJECT TO ADD A NEW TASK]");
            return;
        }
        projectList.sort(Comparator.comparing(Project::getName));
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println((i + 1) + ". " + projectList.get(i).getName());
        }
        System.out.println("ENTER NAME OF PROJECT TO ADD A NEW TASK:");
        while (true) {
            String projectName = ConsoleHelper.readString();
            for (Project project : projectList) {
                if (project.getName().equals(projectName)) {
                    System.out.println("ENTER NAME OF TASK:");
                    String name = ConsoleHelper.readString();
                    System.out.println("ENTER DESCRIPTION:");
                    String description = ConsoleHelper.readString();
                    storage.getTasks().add(new Task(name, description, project));
                    System.out.println(" [TASK ADDED]");
                    return;
                }
            }
            System.out.println(" [NO SUCH PROJECT " + projectName + " ENTER CORRECT NAME]");
        }

    }
}
