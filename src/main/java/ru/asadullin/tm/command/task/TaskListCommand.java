package ru.asadullin.tm.command.task;

import ru.asadullin.tm.Storage;
import ru.asadullin.tm.command.Command;
import ru.asadullin.tm.entity.Task;

import java.util.Comparator;
import java.util.List;

/**
 * Command shows list of tasks in storage
 */
public class TaskListCommand implements Command {
    @Override
    public void execute() {
        System.out.println(" [TASK LIST]");
        Storage storage = Storage.getInstance();
        List<Task> taskList = storage.getTasks();
        if (taskList.isEmpty()) {
            System.out.println(" [TASK LIST IS EMPTY]");
            return;
        }
        taskList.sort(Comparator.comparing(Task::getName));
        for (int i = 0; i < taskList.size(); i++) {
            System.out.println((i + 1) + ". " + taskList.get(i).getName());
        }
        System.out.println();
    }
}
