package ru.asadullin.tm.command.task;

import ru.asadullin.tm.Storage;
import ru.asadullin.tm.command.Command;
import ru.asadullin.tm.entity.Project;
import ru.asadullin.tm.entity.Task;
import ru.asadullin.tm.utils.ConsoleHelper;

import java.util.Comparator;
import java.util.List;

/**
 * Command remove selected task or give you a list of tasks to delete
 */
public class TaskDeleteCommand implements Command {
    @Override
    public void execute() {
        System.out.println(" [TASK DELETE]");
        Storage storage = Storage.getInstance();
        List<Project> projectList = storage.getProjects();
        List<Task> taskList = storage.getTasks();
        if (projectList.isEmpty()) {
            System.out.println(" [PROJECT LIST IS EMPTY]");
            return;
        }
        if (storage.getSelectedTask() == null) {
            taskList.sort(Comparator.comparing(Task::getName));
            for (int i = 0; i < taskList.size(); i++) {
                System.out.println((i + 1) + ". " + taskList.get(i).getName());
            }
            System.out.println();
            System.out.println("ENTER TASK TO REMOVE:");
            String name = ConsoleHelper.readString();
            if (!searchAndDelete(taskList, name))
                System.out.println("NO SUCH TASK EXISTS");
        } else {
            String name = storage.getSelectedTask().getName();
            searchAndDelete(taskList, name);
        }
    }

    private boolean searchAndDelete(List<Task> taskList, String name) {
        for (Task task : taskList) {
            if (task.getName().equals(name)) {
                if (ConsoleHelper.confirmOperation()) {
                    taskList.remove(task);
                    System.out.println(" [TASK DELETED]");
                } else {
                    System.out.println(" [OPERATION CANCELLED]");
                }
                return true;
            }
        }
        return false;
    }
}
