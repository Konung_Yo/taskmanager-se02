package ru.asadullin.tm.command.task;

import ru.asadullin.tm.Storage;
import ru.asadullin.tm.command.Command;

/**
 * Command deselects task in storage
 */
public class TaskDeselectCommand implements Command {
    @Override
    public void execute() {
        System.out.println(" [DESELECT TASK]");
        Storage storage = Storage.getInstance();
        storage.deselectTask();
        System.out.println(" [TASK DESELECTED]");
    }
}
