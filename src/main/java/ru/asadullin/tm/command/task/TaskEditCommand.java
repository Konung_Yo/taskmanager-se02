package ru.asadullin.tm.command.task;

import ru.asadullin.tm.Storage;
import ru.asadullin.tm.command.Command;
import ru.asadullin.tm.entity.Project;
import ru.asadullin.tm.entity.Task;
import ru.asadullin.tm.utils.ConsoleHelper;

import java.util.Comparator;
import java.util.List;

/**
 * Command edit selected task or give you a list of tasks to edit
 */
public class TaskEditCommand implements Command {
    @Override
    public void execute() {
        System.out.println(" [EDIT TASK]");
        Storage storage = Storage.getInstance();
        List<Project> projectList = storage.getProjects();
        List<Task> taskList = storage.getTasks();
        if (projectList.isEmpty()) {
            System.out.println(" [PROJECT LIST IS EMPTY]");
            return;
        }
        if (storage.getSelectedTask() == null) {
            taskList.sort(Comparator.comparing(Task::getName));
            for (int i = 0; i < projectList.size(); i++) {
                System.out.println((i + 1) + ". " + projectList.get(i).getName());
            }
            System.out.println();
            System.out.println("ENTER NAME OF TASK TO EDIT:");
            String inputProject = ConsoleHelper.readString();
            for (Task task : taskList) {
                if (task.getName().equals(inputProject)) {
                    changingNameAndDescription(task);
                    break;
                }
            }
        } else {
            System.out.println("EDIT SELECTED TASK");
            Task task = storage.getSelectedTask();
            changingNameAndDescription(task);
        }
    }

    private void changingNameAndDescription(Task task) {
        System.out.println("ENTER NEW NAME:");
        String name = ConsoleHelper.readString();
        task.setName(name);
        System.out.println("ENTER NEW DESCRIPTION:");
        String description = ConsoleHelper.readString();
        task.setDescription(description);
        System.out.println(" [PROJECT EDITED]");
    }
}
