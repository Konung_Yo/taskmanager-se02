package ru.asadullin.tm.command.task;

import ru.asadullin.tm.Storage;
import ru.asadullin.tm.command.Command;
import ru.asadullin.tm.entity.Project;
import ru.asadullin.tm.entity.Task;
import ru.asadullin.tm.utils.ConsoleHelper;
import ru.asadullin.tm.utils.DateHelper;

import java.util.Comparator;
import java.util.List;

/**
 * Command to select task
 */
public class TaskSelectCommand implements Command {
    @Override
    public void execute() {
        System.out.println(" [SELECT TASK]");
        Storage storage = Storage.getInstance();
        List<Project> projectList = storage.getProjects();
        List<Task> taskList = storage.getTasks();
        if (projectList.isEmpty()) {
            System.out.println(" [PROJECT LIST IS EMPTY]");
            return;
        }
        taskList.sort(Comparator.comparing(Task::getName));
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println((i + 1) + ". " + projectList.get(i).getName());
        }
        System.out.println();
        System.out.println("ENTER NAME OF TASK:");
        String selectedName = ConsoleHelper.readString();
        for (Task task : taskList) {
            if (task.getName().equals(selectedName)) {
                storage.setSelectedTask(task);
                System.out.println("Project: " + task.getProject().getName());
                System.out.println("Name of task: " + task.getName());
                System.out.println("Description of task: " + task.getDescription());
                System.out.println("Date: " + DateHelper.formatDateTime(task.getCreateDate()));
                System.out.println();
                return;
            }
        }
        System.out.println(" [NO SUCH TASK EXISTS]");
    }
}
