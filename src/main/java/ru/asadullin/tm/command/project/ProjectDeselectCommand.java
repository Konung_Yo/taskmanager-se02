package ru.asadullin.tm.command.project;

import ru.asadullin.tm.Storage;
import ru.asadullin.tm.command.Command;

/**
 * Command deselects project in storage
 */
public class ProjectDeselectCommand implements Command {
    @Override
    public void execute() {
        System.out.println(" [DESELECT PROJECT]");
        Storage storage = Storage.getInstance();
        storage.deselectProject();
        System.out.println(" [PROJECT DESELECTED]");
    }
}
