package ru.asadullin.tm.command.project;

import ru.asadullin.tm.Storage;
import ru.asadullin.tm.command.Command;
import ru.asadullin.tm.entity.Project;
import ru.asadullin.tm.entity.Task;
import ru.asadullin.tm.utils.ConsoleHelper;
import ru.asadullin.tm.utils.DateHelper;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Command to select project
 */
public class ProjectSelectCommand implements Command {
    @Override
    public void execute() {
        System.out.println(" [SELECT PROJECT]");
        Storage storage = Storage.getInstance();
        List<Project> projectList = storage.getProjects();
        List<Task> taskList = storage.getTasks();
        if (projectList.isEmpty()) {
            System.out.println(" [PROJECT LIST IS EMPTY]");
            return;
        }
        projectList.sort(Comparator.comparing(Project::getName));
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println((i + 1) + ". " + projectList.get(i).getName());
        }
        System.out.println();
        System.out.println("ENTER NAME OF PROJECT:");
        String selectedName = ConsoleHelper.readString();
        for (Project project : projectList) {
            if (project.getName().equals(selectedName)) {
                storage.setSelectedProject(project);
                System.out.println("Name of project: " + project.getName());
                System.out.println("Description of project: " + project.getDescription());
                System.out.println("Date: " + DateHelper.formatDateTime(project.getCreateDate()));
                List<Task> tasksToShow = new ArrayList<>();
                for (Task task : taskList) {
                    if (task.getProject() == project) {
                        tasksToShow.add(task);
                    }
                }
                if (tasksToShow.isEmpty()) {
                    System.out.println("Task list of project is empty");
                } else {
                    tasksToShow.sort(Comparator.comparing(Task::getName));
                    for (int i = 0; i < tasksToShow.size(); i++) {
                        System.out.println((i + 1) + ". " + tasksToShow.get(i).getName());
                    }
                }
                System.out.println();
                return;
            }
        }
        System.out.println(" [NO SUCH PROJECT EXISTS]");
    }
}
