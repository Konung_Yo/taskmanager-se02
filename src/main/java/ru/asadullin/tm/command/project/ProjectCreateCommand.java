package ru.asadullin.tm.command.project;

import ru.asadullin.tm.Storage;
import ru.asadullin.tm.command.Command;
import ru.asadullin.tm.entity.Project;
import ru.asadullin.tm.utils.ConsoleHelper;

/**
 * Command create new project entity and put it in taskList in storage
 */
public class ProjectCreateCommand implements Command {
    @Override
    public void execute() {
        Storage storage = Storage.getInstance();
        System.out.println(" [PROJECT CREATE]");
        System.out.println("ENTER NAME OF PROJECT:");
        String name = ConsoleHelper.readString();
        System.out.println("ENTER DESCRIPTION:");
        String description = ConsoleHelper.readString();
        storage.getProjects().add(new Project(name, description));
        System.out.println(" [PROJECT ADDED]");
    }
}
