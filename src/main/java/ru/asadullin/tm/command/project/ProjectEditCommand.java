package ru.asadullin.tm.command.project;

import ru.asadullin.tm.Storage;
import ru.asadullin.tm.command.Command;
import ru.asadullin.tm.entity.Project;
import ru.asadullin.tm.utils.ConsoleHelper;

import java.util.Comparator;
import java.util.List;

/**
 * Command edit selected project or give you a list of projects to edit
 */
public class ProjectEditCommand implements Command {
    @Override
    public void execute() {
        System.out.println(" [EDIT PROJECT]");
        Storage storage = Storage.getInstance();
        List<Project> projectList = storage.getProjects();
        if (projectList.isEmpty()) {
            System.out.println(" [PROJECT LIST IS EMPTY]");
            return;
        }
        if (storage.getSelectedProject() == null) {
            projectList.sort(Comparator.comparing(Project::getName));
            for (int i = 0; i < projectList.size(); i++) {
                System.out.println((i + 1) + ". " + projectList.get(i).getName());
            }
            System.out.println();
            System.out.println("ENTER NAME OF PROJECT TO EDIT:");
            String inputProject = ConsoleHelper.readString();
            for (Project project : projectList) {
                if (project.getName().equals(inputProject)) {
                    changingNameAndDescription(project);
                    break;
                }
            }
        } else {
            System.out.println("EDIT SELECTED PROJECT");
            Project project = storage.getSelectedProject();
            changingNameAndDescription(project);
        }
    }

    private void changingNameAndDescription(Project project) {
        System.out.println("ENTER NEW NAME:");
        String name = ConsoleHelper.readString();
        project.setName(name);
        System.out.println("ENTER NEW DESCRIPTION:");
        String description = ConsoleHelper.readString();
        project.setDescription(description);
        System.out.println(" [PROJECT EDITED]");
    }
}
