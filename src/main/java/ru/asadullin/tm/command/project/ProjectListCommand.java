package ru.asadullin.tm.command.project;

import ru.asadullin.tm.Storage;
import ru.asadullin.tm.command.Command;
import ru.asadullin.tm.entity.Project;

import java.util.Comparator;
import java.util.List;

/**
 * Command shows list of projects in storage
 */
public class ProjectListCommand implements Command {
    @Override
    public void execute() {
        System.out.println(" [PROJECT LIST]");
        Storage storage = Storage.getInstance();
        List<Project> projectList = storage.getProjects();
        if (projectList.isEmpty()) {
            System.out.println(" [PROJECT LIST IS EMPTY]");
            return;
        }
        projectList.sort(Comparator.comparing(Project::getName));
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println((i + 1) + ". " + projectList.get(i).getName());
        }
        System.out.println();
    }
}
