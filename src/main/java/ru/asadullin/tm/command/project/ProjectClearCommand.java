package ru.asadullin.tm.command.project;

import ru.asadullin.tm.Storage;
import ru.asadullin.tm.command.Command;
import ru.asadullin.tm.utils.ConsoleHelper;

/**
 * Command remove all projects and tasks in storage
 */
public class ProjectClearCommand implements Command {
    @Override
    public void execute() {
        Storage storage = Storage.getInstance();
        System.out.println(" [CLEAR PROJECTS]");
        if (ConsoleHelper.confirmOperation()) {
            storage.getProjects().clear();
            storage.getTasks().clear();
            System.out.println(" [ALL PROJECTS REMOVED]");
        } else {
            System.out.println(" [OPERATION CANCELLED]");
        }
    }
}
