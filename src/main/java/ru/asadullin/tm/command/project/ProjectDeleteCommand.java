package ru.asadullin.tm.command.project;

import ru.asadullin.tm.Storage;
import ru.asadullin.tm.command.Command;
import ru.asadullin.tm.entity.Project;
import ru.asadullin.tm.entity.Task;
import ru.asadullin.tm.utils.ConsoleHelper;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Command remove selected project or give you a list of projects to delete. Also delete tasks that associated with deleted project
 */
public class ProjectDeleteCommand implements Command {
    @Override
    public void execute() {
        System.out.println(" [PROJECT DELETE]");
        Storage storage = Storage.getInstance();
        List<Project> projectList = storage.getProjects();
        List<Task> taskList = storage.getTasks();
        if (projectList.isEmpty()) {
            System.out.println(" [PROJECT LIST IS EMPTY]");
            return;
        }
        if (storage.getSelectedProject() == null) {
            projectList.sort(Comparator.comparing(Project::getName));
            for (int i = 0; i < projectList.size(); i++) {
                System.out.println((i + 1) + ". " + projectList.get(i).getName());
            }
            System.out.println();
            System.out.println("ENTER PROJECT TO REMOVE:");
            String name = ConsoleHelper.readString();
            if (!searchAndDelete(projectList, taskList, name))
                System.out.println("NO SUCH PROJECT EXISTS");
        } else {
            System.out.println("REMOVE SELECTED PROJECT");
            String name = storage.getSelectedProject().getName();
            searchAndDelete(projectList, taskList, name);
        }
    }

    private boolean searchAndDelete(List<Project> projectList, List<Task> taskList, String name) {
        for (Project project : projectList) {
            if (project.getName().equals(name)) {
                if (ConsoleHelper.confirmOperation()) {
                    List<Task> tasksToDelete = new ArrayList<>();
                    for (Task task : taskList) {
                        if (task.getProject() == project) {
                            tasksToDelete.add(task);
                        }
                    }
                    taskList.removeAll(tasksToDelete);
                    projectList.remove(project);
                    System.out.println(" [PROJECT DELETED]");
                } else {
                    System.out.println(" [OPERATION CANCELLED]");
                }
                return true;
            }
        }
        return false;
    }
}
