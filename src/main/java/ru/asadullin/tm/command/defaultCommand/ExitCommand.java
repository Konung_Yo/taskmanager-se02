package ru.asadullin.tm.command.defaultCommand;

import ru.asadullin.tm.command.Command;

/**
 * Exit from program command
 */
public class ExitCommand implements Command {
    @Override
    public void execute() {
        System.out.println("Good bye!");
    }
}
