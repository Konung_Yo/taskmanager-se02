package ru.asadullin.tm.command.defaultCommand;

import ru.asadullin.tm.Operation;
import ru.asadullin.tm.command.Command;

/**
 * Help command shows all able commands
 */
public class HelpCommand implements Command {
    @Override
    public void execute() {
        for (Operation operation : Operation.values()) {
            System.out.println(operation.toString());
        }
    }
}
