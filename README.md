# Task Manager

### Описание проекта
TASK MANAGER - программа, в которой можно создавать свои проекты и задачи. В каждый проект можно добавлять описание, а так же его редактировать. Каждую задачу можно создавать в общем списке или внутри созданного проекта, так же добавлять задачу в проект. При просмотре проекта или задачи - будет выведено название, описание, время и дата создания.

### Требования к SOFTWARE
```
MacOs
Windows
Linux
```

### Стек технологий
```
IntelliJ IDEA Ultimate
Java SE Development 8
Apache Maven version 4.0.0
Git
```

### Разработчик
```
Асадуллин Эльнур Маратович
Asadelmar@gmail.com
```

### Консольные команды:
```
help: Show all commands.
project-select \"name\": Select project by name.
project-deselect: Deselect last project.
project-edit: Edit selected project.
project-clear: Remove all projects.
project-create: Create new project.
project-list: Show all projects.
project-delete: delete project.
task-select \"name\": Select project by name.
task-deselect: Deselect last task.
task-edit: Edit selected project.
task-clear: Remove all tasks.
task-create: Create new task.
task-list: Show all tasks.
task-delete: delete task.
exit: Exit the Task manager
```
